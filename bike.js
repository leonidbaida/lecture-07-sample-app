// bike.js

window.addEventListener('load', function () {
    var bike = bikes[Utils.getParams().bikeId];

    var h1 = Utils.creteElement('h3', document.body);
    h1.textContent = bike.vendor + ' ' + bike.name;

    var img = Utils.creteElement('img', document.body);
    img.src = './image/' + bike.image;
});